# Visioton

This is a simple Firefox content script that hides HS Visio section from Helsingin Sanomat website.

## Building, packaging etc

The extension is written in vanilla JS and no build tools are used. In order to produce the extension package just use any zip archiver and package the files in this repository.
