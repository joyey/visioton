const visioLink = /\/visio\//

function hideNavBarVisioContent() {
	const navBarList = document.getElementById('header').getElementsByTagName('ul');
	processLists(navBarList);
}

function hideAsideVisioContent() {
	const aside = document.getElementsByTagName('aside').item(0);
	const asideLists = aside.getElementsByTagName('ol');
	processLists(asideLists);
}

function hideMainVisioContent() {
	const main = document.getElementsByTagName('main').item(0);
	const articles = main.getElementsByTagName('article');
	for (const article of Array.from(articles)) {
		if (containsVisioLink(article)) {
			article.hidden = true;
		}
	}

	// most read, latest etc
	const lists = main.getElementsByTagName('ul');
	processLists(lists);
}

function processLists(listColl) {
	for (const list of Array.from(listColl)) {
		for (const item of Array.from(list.getElementsByTagName('li'))) {
			if (containsVisioLink(item)) {
				item.hidden = true;
			}
		}
	}
}

function containsVisioLink(e) {
	const links = e.getElementsByTagName('a');
	for (const link of links) {
		if (visioLink.test(link.href)) {
			return true;
		}
	}
	return false;
}

function mutationHandler(mutations, observer) {
	const lists = [];
	for (const mutation of Array.from(mutations)) {
		const listsToCheck = Array.from(mutation.addedNodes).filter(n => n.tagName.toLowerCase() === 'li').map(li => li.parentNode);
		if (listsToCheck.length > 0) {
			for (parent of listsToCheck) {
				if (!lists.includes(parent)) {
					lists.push(parent);
				}
			}
		}

		const divs = Array.from(mutation.addedNodes).filter(n => n.tagName.toLowerCase() === 'div');
		if (divs.length > 0) {
			divs.forEach(div => {
				const articles = Array.from(div.getElementsByTagName('article'));
				articles.forEach(a => {
					if (containsVisioLink(a)) {
						a.hidden = true;
					}
				});
			});
		}
	}

	if (lists.length > 0) {
		processLists(lists);
	}
}

function hideCrap() {
	hideNavBarVisioContent();
	hideMainVisioContent();
	hideAsideVisioContent();
}

window.onload = function() {
	hideCrap();
	const next = document.getElementById('__next');
	const observer = new MutationObserver(mutationHandler);
	observer.observe(next, {childList: true, subtree: true});
}
